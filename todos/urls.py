from django.urls import path
from todos.views import (
    TodoListListView,
    TodoListDetailView,
    TodoListCreateView,
    TodoListUpdateView,
    TodoListDeleteView,
    TodoItemCreateView,
    TodoItemUpdateView,
)


urlpatterns = [
    path("", TodoListListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_new"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todo_list_update"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_list_delete"),
    path("item/create/", TodoItemCreateView.as_view(), name="todo_item_new"),
    path("<int:pk>/items/edit/", TodoItemUpdateView.as_view(), name="todo_item_update"),
]
